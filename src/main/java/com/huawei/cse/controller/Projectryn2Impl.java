package com.huawei.cse.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-01-02T08:29:49.822Z")

@RestSchema(schemaId = "projectryn2")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class Projectryn2Impl {

    @Autowired
    private Projectryn2Delegate userProjectryn2Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjectryn2Delegate.helloworld(name);
    }

}
